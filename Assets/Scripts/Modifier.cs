﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Modifier {
    public string name;
    public enum StatModifierType
    {
        AttackSpeed, Range, CritChance, CritMultiplier, PhysDamage,
        BleedChance, BleedDamage, FireDamage, BurnChance, BurnDamage, ColdDamage,
        FrostbiteChance, FrostbiteStrength, ElectricDamage, StunChance, StunDuration, ProjectileChain, ProjectileSplash, ProjectilePierce, ProjectileSpeed
    };
    public StatModifierType statModifierType;
    public enum HowItModifies { increase, decrease, more, less, flatAdditional }; //how it modifies
    public HowItModifies howItModifies; //what type of modifier is is. The AdditionalDamage type modifiers only take flatAdditional Type.





    public int modifierTier; //The tier of the modifier. This is which range for the modifier it gets.

    public float modifierAmount; //The value of the modifier
    public int modifierRangeMinimum;//The stored minimum range value, for rerolling ranges.
    public int modifierRangeMaximum;//The stored maximum range value, for rerolling ranges.

    public float secondaryModifierAmout;//only used when needed like adding to min and max damage.
    public int secondaryModifierRangeMinimum;//The stored secondary minimum range value, for rerolling ranges. Only used for certain mods.
    public int secondaryModifierRangeMaximum;//The stored secondary maximum range value, for rerolling ranges. Only used for certain mods.
    public string getReadableDescription()
    {
        string returnedString = "";
        switch (howItModifies)
        {
            case HowItModifies.increase:
                returnedString += modifierAmount + "% " + "Increased ";
                break;

            case HowItModifies.decrease:
                returnedString += modifierAmount + "% " + "Decreased ";
                break;

            case HowItModifies.more:
                returnedString += modifierAmount + "% " + "More ";
                break;

            case HowItModifies.less:
                returnedString += modifierAmount + "% " + "Less ";
                break;

            case HowItModifies.flatAdditional: //Special case, needs to handle it in here
                switch(statModifierType)
                {
                    case StatModifierType.AttackSpeed:
                        returnedString += "Adds " + modifierAmount + " to Attack Speed";
                        break;

                    case StatModifierType.Range:
                        returnedString += "Adds " + modifierAmount + " to Tower Range";
                        break;

                    case StatModifierType.CritChance:
                        returnedString += "Adds " + modifierAmount + "% to Critical Strike Chance";
                        break;

                    case StatModifierType.CritMultiplier:
                        returnedString += "Adds " + modifierAmount + "% to Critical Strike Multiplier";
                        break;

                    case StatModifierType.PhysDamage:
                        returnedString += "Adds " + modifierAmount + " to " + secondaryModifierAmout + " ";
                        break;

                    case StatModifierType.BleedChance:
                        returnedString += "Adds " + modifierAmount + "% to Bleed Chance";
                        break;

                    case StatModifierType.BleedDamage:
                        returnedString += "Adds " + modifierAmount + " to Bleed Damage";
                        break;

                    case StatModifierType.FireDamage:
                        returnedString += "Adds " + modifierAmount + " to " + secondaryModifierAmout + " ";
                        break;

                    case StatModifierType.BurnChance:
                        returnedString += "Adds " + modifierAmount + "% to Burn Chance";
                        break;

                    case StatModifierType.BurnDamage:
                        returnedString += "Adds " + modifierAmount + "% to Burn Damage";
                        break;

                    case StatModifierType.ColdDamage:
                        returnedString += "Adds " + modifierAmount + " to " + secondaryModifierAmout + " ";
                        break;

                    case StatModifierType.FrostbiteChance:
                        returnedString += "Adds " + modifierAmount + "% to Frostbite Chance";
                        break;

                    case StatModifierType.FrostbiteStrength:
                        returnedString += "Adds " + modifierAmount + "% to Frostbite Strength";
                        break;

                    case StatModifierType.ElectricDamage:
                        returnedString += "Adds " + modifierAmount + " to " + secondaryModifierAmout + " ";
                        break;

                    case StatModifierType.StunChance:
                        returnedString += "Adds " + modifierAmount + "% to Stun Chance";
                        break;

                    case StatModifierType.StunDuration:
                        returnedString += "Adds " + modifierAmount + " to Stun Duration";
                        break;

                    case StatModifierType.ProjectileChain:
                        returnedString += "Projectiles Chain " + modifierAmount + " More Times";
                        break;

                    case StatModifierType.ProjectileSplash:
                        returnedString += "Adds " + modifierAmount + " to Projectile Splash";
                        break;

                    case StatModifierType.ProjectilePierce:
                        returnedString += "Projectiles Pierce " + modifierAmount + " More Targets";
                        break;

                    case StatModifierType.ProjectileSpeed:
                        returnedString += "Adds " + modifierAmount + " to Projectile Speed";
                        break;

                }
                break;
        }
        switch (statModifierType)
        {
            case StatModifierType.AttackSpeed:
                returnedString += "Attack Speed";
                break;

            case StatModifierType.Range:
                returnedString += "Tower Range";
                break;

            case StatModifierType.CritChance:
                returnedString += "Critical Strike Chance";
                break;

            case StatModifierType.CritMultiplier:
                returnedString += "Critical Strike Multiplier";
                break;

            case StatModifierType.PhysDamage:
                returnedString += "Physical Damage";
                break;

            case StatModifierType.BleedChance:
                returnedString += "Bleed Chance";
                break;

            case StatModifierType.BleedDamage:
                returnedString += "Bleed Damage";
                break;

            case StatModifierType.FireDamage:
                returnedString += "Fire Damage";
                break;

            case StatModifierType.BurnChance:
                returnedString += "Burn Chance";
                break;

            case StatModifierType.BurnDamage:
                returnedString += "Burn Damage";
                break;

            case StatModifierType.ColdDamage:
                returnedString += "Cold Damage";
                break;

            case StatModifierType.FrostbiteChance:
                returnedString += "Frostbite Chance";
                break;

            case StatModifierType.FrostbiteStrength:
                returnedString += "Frostbite Strength";
                break;

            case StatModifierType.ElectricDamage:
                returnedString += "Electrical Damage";
                break;

            case StatModifierType.StunChance:
                returnedString += "Stun Chance";
                break;

            case StatModifierType.StunDuration:
                returnedString += "Stun Duration";
                break;

            case StatModifierType.ProjectileChain:
                returnedString += "Number of Chains";
                break;

            case StatModifierType.ProjectileSplash:
                returnedString += "Splash Radius";
                break;

            case StatModifierType.ProjectilePierce:
                returnedString += "Number of Projectiles";
                break;

            case StatModifierType.ProjectileSpeed:
                returnedString += "Projectile Speed";
                break;
        }

        return returnedString;
    }
    //Each modifier will have an array of these, they are the stat tiers

    public void RerollModifierValue() //Function to reroll the value of the modifier within the same range.
    {
        modifierAmount = Random.Range(modifierRangeMinimum, modifierRangeMaximum + 1);
        secondaryModifierAmout = Random.Range(secondaryModifierRangeMinimum, secondaryModifierRangeMaximum + 1);
    }
}
