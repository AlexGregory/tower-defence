﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifier Database")]
// Use this for initialization
public class ModifierDatabase : ScriptableObject {
    public ItemLevelTier[] TierArray;


    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    [System.Serializable]
    public class ItemLevelTier
    {
        public int tier; // The tier this tier is
        public int minimumLevelToRollTier; // what level must the tower be to get a chance to roll this tier
        public int modifierMinValue;//The minimum value of this tier 
        public int modifierMaxValue;//The maximum value of this tier 

        //Secondary values only needed for some modifiers
        public int secondModifierMinValue;//The minimum secondary value of this tier 
        public int secondModifierMaxValue;//The maximum secondary value of this tier 
    }
    //This function takes in a tower level and return a random modifier, but the increase/decrease is not yet decided.
    public Modifier ReturnAppropriateModifierTier(int level)
    {
        //Iterate through the different tiers, starting at the bottom, iterating possible tiers for each tier that is possible
        int possibleTiers = 0;
        for (int i = 0; i < TierArray.Length; i++)
        {
            //if the provided level is equal to or greater than the current tier
            if (level >= TierArray[i].minimumLevelToRollTier)
            {
                //iterate possible tiers
                possibleTiers++;
            }
        }

        int chosenTier = Random.Range(1, possibleTiers + 1);
        ItemLevelTier temp = TierArray[chosenTier - 1];

        Modifier modifier = new Modifier();

        //set each variable
        modifier.modifierRangeMinimum = temp.modifierMinValue; // min value
        modifier.modifierRangeMaximum = temp.modifierMaxValue; // max value
        modifier.secondaryModifierRangeMinimum = temp.secondModifierMinValue; // secondary min value
        modifier.secondaryModifierRangeMaximum = temp.modifierMaxValue; // secondary max value
        modifier.modifierTier = temp.tier; // tier number
        modifier.modifierAmount = Random.Range(modifier.modifierRangeMinimum, modifier.modifierRangeMaximum + 1); // set value
        modifier.secondaryModifierAmout = Random.Range(modifier.secondaryModifierRangeMinimum, modifier.secondaryModifierRangeMaximum + 1); // set secondary value
        //returns the modifier, it has no type or stat yet, it is pure numbers
        return modifier;
    }
}
