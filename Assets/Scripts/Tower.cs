﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public enum Rarity { Common, Uncommon, Rare, Epic, Legendary };

   // public Modifier[] towerModifiers;
    public List<Modifier> towerModifiers = new List<Modifier>();
    public AttackSpeed attackSpeed;
    public Range range;
    public CritChance critChance;
    public CritMultiplier critMultiplier;
    public PhysMinDamage physMinDamage;
    public PhysMaxDamage physMaxDamage;
    public BleedChance bleedChance;
    public BleedDamage bleedDamage;
    public FireMinDamage fireMinDamage;
    public FireMaxDamage fireMaxDamage;
    public BurnChance burnChance;
    public BurnDamage burnDamage;
    public ColdMinDamage coldMinDamage;
    public ColdMaxDamage coldMaxDamage;
    public FrostbiteChance frostbiteChance;
    public FrostbiteStrength frostbiteStrength;
    public ElectricMinDamage electricMinDamage;
    public ElectricMaxDamage electricMaxDamage;
    public StunChance stunChance;
    public StunDuration stunDuration;
    public ProjectileChain projectileChain;
    public ProjectilePierce projectilePierce;
    public ProjectileSpeed projectileSpeed;
    public ProjectileSplash projectileSplash;
    public TowerData towerData;
    public Rarity towerRarity; //The current rarity of the tower.

    //This clas contains the booleans for if a tower does each type of elemental damage, having atleast stome of an element gives the base chance for status effects to occur
    [System.Serializable]
    public class TowerData 
    {
        public int itemLevel;
        public bool towerDoesPhysDamage;
        public bool towerDoesFireDamage;
        public bool towerDoesColdDamage;
        public bool towerDoesElectricalDamage;
        public bool towerHasSplashArea;
        public bool towerPierces;
        public bool towerChains;
    }

    [System.Serializable]
    public class AttackSpeed
    {
        public float starting; //starting attack Speed of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final attack Speed of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class Range
    {
        public float starting; //starting attack range of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final attack range of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class CritChance
    {
        public float starting; //starting crit chance of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final crit chance of the tower

        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class CritMultiplier
    {
        public float starting; //starting crit damage multiplier of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final crit damage multiplier of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class PhysMinDamage
    {
        public float starting; //starting minimum physical damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final minimum physical damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class PhysMaxDamage
    {
        public float starting; //starting max phsical damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final max phsical damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class BleedChance
    {
        public float starting; //starting chance to cause bleed damage - Damage over time
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final chance to cause bleed damage - Damage over time
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class BleedDamage
    {
        public float starting; //starting bleed damage as a percentage of original hit
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final bleed damage as a percentage of original hit
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class FireMinDamage
    {
        public float starting; //starting minimum Fire damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final minimum Fire damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class FireMaxDamage
    {
        public float starting; //starting max Fire damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final max Fire damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class BurnChance
    {
        public float starting; //starting chance to burn on hit - burning for additional damage
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final chance to burn on hit - burning for additional damage
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class BurnDamage
    {
        public float starting; //starting burn damage - as % max health?
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final burn damage - as % max health?
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ColdMinDamage
    {
        public float starting; //starting minimum Cold damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final minimum Cold damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ColdMaxDamage
    {
        public float starting; //starting max Cold damage of the tower]
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final max Cold damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class FrostbiteChance
    {
        public float starting; //starting chance to frostbite on hit - reduce armor/increase damage
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final chance to frostbite on hit - reduce armor/increase damage
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class FrostbiteStrength
    {
        public float starting; //starting extra % damage do things take
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final extra % damage do things take
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ElectricMinDamage
    {
        public float starting; //starting minimum Electric damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final minimum Electric damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ElectricMaxDamage
    {
        public float starting; //starting max Electric damage of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final max Electric damage of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class StunChance
    {
        public float starting; //starting chance to stun - stop all movement
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final chance to stun - stop all movement
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class StunDuration
    {
        public float starting; //starting stun duration
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final stun duration
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable] 
    public class ProjectileChain //Projectile Chain will round down to whole integers
    {
        public float starting; //starting number of times a projectile chains of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final number of times a projectile chains of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ProjectileSplash
    {
        public float starting; //starting splash size of the tower
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final splash size of the tower
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]//Projectile pierce will round down to whole integers
    public class ProjectilePierce
    {
        public float starting; //starting number of times a projectile will pierce.
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final number of times a projectile will pierce.
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    [System.Serializable]
    public class ProjectileSpeed
    {
        public float starting; //starting speed of the projectile
        public float flatAdded; //An added flat value that it added to the starting before calculations, only certain things will need this.
        public float addativeModifier; // The additive modifier amount - Used to calculate the total when going through modifiers
        public float multiplicitiveModifier; //The multiplicitive modifier amount - Used to calculate the total when going through modifiers
        public float final; //final speed of the projectile
        public void GetFinal()
        {
            final = (starting + flatAdded) * addativeModifier * multiplicitiveModifier;
        }
    }
    



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            ApplyModifiers();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            AddBasicFlatDamageModifier();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            AddBasicDamageModifier();
        }


        
    }
    //function to add damage flat damage in one of the four types. It is the first 2 modifiers that are run
    public void AddBasicFlatDamageModifier()
    {
        //Roll a die with a side for each modifier
        int die = Random.Range(1, 5);
        Modifier createdModifier = new Modifier();
        switch (die)
        {

            case 1: //Cold Damage
                createdModifier = GameManager.instance.modifierDatabases.FlatColdDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.ColdDamage;
                createdModifier.howItModifies = Modifier.HowItModifies.flatAdditional;
                towerData.towerDoesColdDamage = true;
                break;

            case 2: //Electric Damage
                createdModifier = GameManager.instance.modifierDatabases.FlatElectricalDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.ElectricDamage;
                createdModifier.howItModifies = Modifier.HowItModifies.flatAdditional;
                towerData.towerDoesElectricalDamage = true;
                break;

            case 3: //Fire Damage
                createdModifier = GameManager.instance.modifierDatabases.FlatFireDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.FireDamage;
                createdModifier.howItModifies = Modifier.HowItModifies.flatAdditional;
                towerData.towerDoesFireDamage = true;
                break;

            case 4: //Physical Damage
                createdModifier = GameManager.instance.modifierDatabases.FlatPhysicalDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.PhysDamage;
                createdModifier.howItModifies = Modifier.HowItModifies.flatAdditional;
                towerData.towerDoesPhysDamage = true;
                break;

        }

            towerModifiers.Add(createdModifier);

        //Debug.Log( createdModifier.getReadableDescription());
    }
    //Modifiers that can add flat damage or increase/Decrease
    public void AddBasicDamageModifier()
    {
        //Create a list that starts empty;
         List<string> modifierList = new List<string>();
         //We are then going to add strings to the list based on what types of damage it does
        //Add status related modifiers if applicable
        if(towerData.towerDoesPhysDamage == true)
        { 
            modifierList.Add("Physical Damage");
            modifierList.Add("Bleed Chance");
            modifierList.Add("Bleed Damage");
        }

        if (towerData.towerDoesColdDamage == true)
        {
            modifierList.Add("Cold Damage");
            modifierList.Add("Frostbite Chance");
            modifierList.Add("Frostbite Strength");
        }

        if (towerData.towerDoesElectricalDamage == true)
        {
            modifierList.Add("Electric Damage");
            modifierList.Add("Stun Chance");
            modifierList.Add("Stun Duration");
        }

        if (towerData.towerDoesFireDamage == true)
        {
            modifierList.Add("Fire Damage");
            modifierList.Add("Burn Chance");
            modifierList.Add("Burn Damage");
        }
        //Roll a die with a number of sides equal to the amount of strings we added to the list
        int die = Random.Range(0, modifierList.Count);
        string chosenString = modifierList[die];
        Modifier createdModifier = new Modifier();
        //we now have a switch statement that has all possible choices, but we only randomly get those that are applicable
        switch (chosenString)
        {

            case "Cold Damage": //Cold Damage
                createdModifier = GameManager.instance.modifierDatabases.ColdDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.ColdDamage;
                towerData.towerDoesColdDamage = true;
                break;

            case "Electric Damage": //Electric Damage
                createdModifier = GameManager.instance.modifierDatabases.ElectricDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.ElectricDamage;
                towerData.towerDoesElectricalDamage = true;
                break;

            case "Fire Damage": //Fire Damage
                createdModifier = GameManager.instance.modifierDatabases.FireDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.FireDamage;
                towerData.towerDoesFireDamage = true;
                break;

            case "Physical Damage": //Physical Damage
                createdModifier = GameManager.instance.modifierDatabases.PhysDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.PhysDamage;
                towerData.towerDoesPhysDamage = true;
                break;

            case "Bleed Chance": //Bleed Chance
                createdModifier = GameManager.instance.modifierDatabases.BleedChance.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.BleedChance;
                break;

            case "Bleed Damage": //Bleed Damage
                createdModifier = GameManager.instance.modifierDatabases.BleedDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.BleedDamage;
                break;

            case "Frostbite Chance": //Frostbite Chance
                createdModifier = GameManager.instance.modifierDatabases.FrostbiteChance.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.FrostbiteChance;
                break;

            case "Frostbite Strength": //Frostbite Strength
                createdModifier = GameManager.instance.modifierDatabases.FrostbiteStrength.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.FrostbiteStrength;
                break;

            case "Stun Chance": //Stun Chance
                createdModifier = GameManager.instance.modifierDatabases.StunChance.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.StunChance;
                break;

            case "Stun Duration": //Stun Duration
                createdModifier = GameManager.instance.modifierDatabases.StunDuration.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.StunDuration;
                break;

            case "Burn Chance": //Burn Chance
                createdModifier = GameManager.instance.modifierDatabases.BurnChance.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.BurnChance;
                break;

            case "Burn Damage": //Burn Damage
                createdModifier = GameManager.instance.modifierDatabases.BurnDamage.ReturnAppropriateModifierTier(towerData.itemLevel);
                createdModifier.statModifierType = Modifier.StatModifierType.BurnDamage;
                break;

        }

        towerModifiers.Add(createdModifier);

        //Debug.Log( createdModifier.getReadableDescription());
    }
    public void ApplyModifiers() //Function to apply each modifier the tower has in its list
    {   //iterate through all the modifiers one by one, getting the correct values for modifiers
        for (int i = 0; i < towerModifiers.Count; i++) 
        {
            //Nested Switch Case, First Switch is startingd on what stat is modified
            switch (towerModifiers[i].statModifierType)
            {
                case Modifier.StatModifierType.AttackSpeed:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            attackSpeed.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            attackSpeed.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            attackSpeed.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            attackSpeed.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            attackSpeed.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.Range:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            range.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            range.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            range.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            range.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            range.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.CritChance:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            critChance.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            critChance.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            critChance.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            critChance.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            critChance.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.CritMultiplier:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            critMultiplier.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            critMultiplier.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            critMultiplier.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            critMultiplier.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            critMultiplier.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.PhysDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            physMinDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            physMaxDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesPhysDamage = true;
                            break;

                        case Modifier.HowItModifies.decrease:
                            physMinDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            physMaxDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesPhysDamage = true;
                            break;

                        case Modifier.HowItModifies.more:
                            physMinDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            physMaxDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesPhysDamage = true;
                            break;

                        case Modifier.HowItModifies.less:
                            physMinDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            physMaxDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesPhysDamage = true;
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            physMinDamage.flatAdded += towerModifiers[i].modifierAmount;
                            physMaxDamage.flatAdded += towerModifiers[i].secondaryModifierAmout;
                            towerData.towerDoesPhysDamage = true;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.BleedChance:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            bleedChance.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            bleedChance.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            bleedChance.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            bleedChance.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            bleedChance.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.BleedDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            bleedDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            bleedDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            bleedDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            bleedDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            bleedDamage.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.FireDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            fireMinDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            fireMaxDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesFireDamage = true;
                            break;

                        case Modifier.HowItModifies.decrease:
                            fireMinDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            fireMaxDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesFireDamage = true;
                            break;

                        case Modifier.HowItModifies.more:
                            fireMinDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            fireMaxDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesFireDamage = true;
                            break;

                        case Modifier.HowItModifies.less:
                            fireMinDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            fireMaxDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesFireDamage = true;
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            fireMinDamage.flatAdded += towerModifiers[i].modifierAmount;
                            fireMaxDamage.flatAdded += towerModifiers[i].secondaryModifierAmout;
                            towerData.towerDoesFireDamage = true;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.BurnChance:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            burnChance.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            burnChance.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            burnChance.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            burnChance.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            burnChance.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.BurnDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            burnDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            burnDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            burnDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            burnDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            burnDamage.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ColdDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            coldMinDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            coldMaxDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesColdDamage = true;
                            break;

                        case Modifier.HowItModifies.decrease:
                            coldMinDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            coldMaxDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesColdDamage = true;
                            break;

                        case Modifier.HowItModifies.more:
                            coldMinDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            coldMaxDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesColdDamage = true;
                            break;

                        case Modifier.HowItModifies.less:
                            coldMinDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            coldMaxDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesColdDamage = true;
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            coldMinDamage.flatAdded += towerModifiers[i].modifierAmount;
                            coldMaxDamage.flatAdded += towerModifiers[i].secondaryModifierAmout;
                            towerData.towerDoesColdDamage = true;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.FrostbiteChance:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            frostbiteChance.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            frostbiteChance.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            frostbiteChance.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            frostbiteChance.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            frostbiteChance.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.FrostbiteStrength:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            frostbiteStrength.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            frostbiteStrength.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            frostbiteStrength.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            frostbiteStrength.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            frostbiteStrength.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ElectricDamage:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            physMinDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            physMaxDamage.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesElectricalDamage = true;
                            break;

                        case Modifier.HowItModifies.decrease:
                            physMinDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            physMaxDamage.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            towerData.towerDoesElectricalDamage = true;
                            break;

                        case Modifier.HowItModifies.more:
                            physMinDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            physMaxDamage.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesElectricalDamage = true;
                            break;

                        case Modifier.HowItModifies.less:
                            physMinDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            physMaxDamage.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            towerData.towerDoesElectricalDamage = true;
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            physMinDamage.flatAdded += towerModifiers[i].modifierAmount;
                            physMaxDamage.flatAdded += towerModifiers[i].secondaryModifierAmout;
                            towerData.towerDoesElectricalDamage = true;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.StunChance:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            stunChance.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            stunChance.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            stunChance.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            stunChance.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            stunChance.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.StunDuration:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            stunDuration.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            stunDuration.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            stunDuration.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            stunDuration.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            stunDuration.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ProjectileChain:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            projectileChain.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            projectileChain.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            projectileChain.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            projectileChain.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            projectileChain.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ProjectileSplash:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            projectileSplash.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            projectileSplash.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            projectileSplash.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            projectileSplash.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            projectileSplash.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ProjectilePierce:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            projectilePierce.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            projectilePierce.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            projectilePierce.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            projectilePierce.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            projectilePierce.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;

                case Modifier.StatModifierType.ProjectileSpeed:
                    switch (towerModifiers[i].howItModifies)
                    {
                        case Modifier.HowItModifies.increase:
                            projectileSpeed.addativeModifier += (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.decrease:
                            projectileSpeed.addativeModifier -= (towerModifiers[i].modifierAmount / 100);
                            break;

                        case Modifier.HowItModifies.more:
                            projectileSpeed.multiplicitiveModifier *= (1 + (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.less:
                            projectileSpeed.multiplicitiveModifier *= (1 - (towerModifiers[i].modifierAmount / 100));
                            break;

                        case Modifier.HowItModifies.flatAdditional:
                            projectileSpeed.flatAdded += towerModifiers[i].modifierAmount;
                            break;
                    }
                    break;
            }
            //debug the readout of the modifier
            Debug.Log(towerModifiers[i].getReadableDescription());
        }

        //Apply them to get final values
        GetFinalStatValues();
    }
    //This function runs getFinal for all stats
    public void GetFinalStatValues()
    {
        checkDamageTypes();
        attackSpeed.GetFinal();
        range.GetFinal();
        critChance.GetFinal();
        critMultiplier.GetFinal();
        physMinDamage.GetFinal();
        physMaxDamage.GetFinal();
        bleedChance.GetFinal();
        bleedDamage.GetFinal();
        fireMinDamage.GetFinal();
        fireMaxDamage.GetFinal();
        burnChance.GetFinal();
        burnDamage.GetFinal();
        coldMinDamage.GetFinal();
        coldMaxDamage.GetFinal();
        frostbiteChance.GetFinal();
        frostbiteStrength.GetFinal();
        electricMinDamage.GetFinal();
        electricMaxDamage.GetFinal();
        stunChance.GetFinal();
        stunDuration.GetFinal();
        projectileChain.GetFinal();
        projectileSplash.GetFinal();
        projectilePierce.GetFinal();
        projectileSpeed.GetFinal();
    }
    
    //Checks if the tower does each type of damage, if it it does it sets the base chacne for inflicting the status to 5%
    public void checkDamageTypes()
    {
       
        if (towerData.towerDoesPhysDamage == true) //Phys damage for bleed
        {
            bleedChance.starting = 5;
        }
        if (towerData.towerDoesFireDamage == true) //Fire damage for burn
        {
            burnChance.starting = 5;
        }
        if (towerData.towerDoesColdDamage == true) //Cold damage for frostbite
        {
            frostbiteChance.starting = 5;
        }
        if (towerData.towerDoesElectricalDamage == true) //Electric damage for stun
        {
            stunChance.starting = 5;
        }
    }
    //Function to roll the tower's rarity.
    public void rollRarity()
    {

    }
}
