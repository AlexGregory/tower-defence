﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public Databases modifierDatabases;


    public static GameManager instance = null;
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        
}


    // Use this for initialization
    void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		
	}
    [System.Serializable]
    public class Databases
    {
        public ModifierDatabase AttackSpeed; //Refernce to the Modifier Database
        public ModifierDatabase Range; //Refernce to the Modifier Database
        public ModifierDatabase CritChance; //Refernce to the Modifier Database
        public ModifierDatabase CritMultiplier; //Refernce to the Modifier Database
        public ModifierDatabase PhysDamage; //Refernce to the Modifier Database
        public ModifierDatabase BleedChance; //Refernce to the Modifier Database
        public ModifierDatabase BleedDamage; //Refernce to the Modifier Database
        public ModifierDatabase FireDamage; //Refernce to the Modifier Database
        public ModifierDatabase BurnChance; //Refernce to the Modifier Database
        public ModifierDatabase BurnDamage; //Refernce to the Modifier Database
        public ModifierDatabase ColdDamage; //Refernce to the Modifier Database
        public ModifierDatabase FrostbiteChance; //Refernce to the Modifier Database
        public ModifierDatabase FrostbiteStrength; //Refernce to the Modifier Database
        public ModifierDatabase ElectricDamage; //Refernce to the Modifier Database
        public ModifierDatabase StunChance; //Refernce to the Modifier Database
        public ModifierDatabase StunDuration; //Refernce to the Modifier Database
        public ModifierDatabase ProjectileChain; //Refernce to the Modifier Database
        public ModifierDatabase ProjectileSplash; //Refernce to the Modifier Database
        public ModifierDatabase ProjectilePierce; //Refernce to the Modifier Database
        public ModifierDatabase ProjectileSpeed; //Refernce to the Modifier Database
        public ModifierDatabase FlatPhysicalDamage; //Refernce to the Modifier Database
        public ModifierDatabase FlatColdDamage; //Refernce to the Modifier Database
        public ModifierDatabase FlatFireDamage; //Refernce to the Modifier Database
        public ModifierDatabase FlatElectricalDamage; //Refernce to the Modifier Database
    }
}
